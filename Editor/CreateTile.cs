using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Editor
{
    public class CreateTile
    {
        [MenuItem("Assets/Create/TileLevelGenerator/Create Tile")]
        static void Create()
        {
            Object[] selection = Selection.objects;
            String path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(selection[0]));

            string guid = AssetDatabase.CreateFolder(path, "Tile");
            string newFolderPath = AssetDatabase.GUIDToAssetPath(guid);
            
            
        }
    }
}