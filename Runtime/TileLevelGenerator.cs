using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Deaven.TileLevelGenerator
{
    public class TileLevelGenerator : MonoBehaviour
    {
        public Vector3 LevelSize { get; private set; }

        public Vector3 LevelCenter { get; private set; }

        public Level[] availableLevel;

        public event Action LevelGenerated;

        private void Start()
        {
            CreateRandomLevel();
        }

        [Button]
        public void CreateRandomLevel()
        {
            int random = Random.Range(0, availableLevel.Length);

            CreateLevel(availableLevel[random]);
        }

        [Button]
        public void CreateLevel(Level level)
        {
            RemoveLevel();

            Vector3 tileSize = level.defaultTile.TilePrefab.transform.localScale;

            for (int y = 0; y < level.layoutTexture.height; y++)
            {
                for (int x = 0; x < level.layoutTexture.width; x++)
                {
                    Color layoutPixelColor = level.layoutTexture.GetPixel(x, y);
                    Color heightPixelColor = level.heightTexture.GetPixel(x, y);

                    TileData tileData = GetTileData(layoutPixelColor, level);
                    int heightLevel = GetHeightLevel(heightPixelColor, level);

                    Vector3Int pos = new Vector3Int(x, heightLevel, y);

                    CreateTileStack(tileSize, tileData, pos, level);
                }
            }

            LevelSize = new Vector3(level.layoutTexture.width * tileSize.x, tileSize.y,
                level.layoutTexture.height * tileSize.z);
            LevelCenter = new Vector3(LevelSize.x / 2, 0, LevelSize.z / 2);

            LevelGenerated?.Invoke();
        }

        [Button]
        public void RemoveLevel()
        {
            var tempArray = new GameObject[transform.childCount];

            for (int i = 0; i < tempArray.Length; i++)
            {
                tempArray[i] = transform.GetChild(i).gameObject;
            }

            foreach (var child in tempArray)
            {
                DestroyImmediate(child);
            }
        }

        protected int GetHeightLevel(Color color, Level level)
        {
            return Mathf.FloorToInt(color.grayscale * level.maxHeight);
        }

        protected void CreateTileStack(Vector3 scale, TileData tileData, Vector3Int pos, Level level)
        {
            GameObject tileGameObject = CreateTile(scale, tileData.TilePrefab, pos);

            for (int i = pos.y - 1; i >= 0; i--)
            {
                GameObject tileToSpawn;
                
                if (tileData.TileBeneathPrefab)
                {
                    tileToSpawn = CreateTile(scale, tileData.TileBeneathPrefab, new Vector3(pos.x, i, pos.z));
                }
                else
                {
                    tileToSpawn = CreateTile(scale, level.defaultTile.TilePrefab, new Vector3(pos.x, i, pos.z));
                }
                

                tileToSpawn.name = $"{tileToSpawn.name} : {i} ";

                Tile tile = tileToSpawn.GetComponent<Tile>();
                tile.Occupied = tileGameObject;
                tileGameObject = tileToSpawn;
            }
        }

        protected GameObject CreateTile(Vector3 scale, GameObject tilePrefab, Vector3 pos)
        {
            GameObject tileGameObject = Instantiate(tilePrefab,
                new Vector3(pos.x * scale.x, pos.y * scale.y, pos.z * scale.z), Quaternion.identity,
                transform);

            tileGameObject.name = $"{tileGameObject.name}: {pos.x} : {pos.z}";

            return tileGameObject;
        }

        protected TileData GetTileData(Color color, Level level)
        {
            foreach (TileData block in level.tiles)
            {
                if (ColorEqual(block.color, color))
                    return block;
            }

            return level.defaultTile;
        }

        private bool ColorEqual(Color color1, Color color2)
        {
            float threshold = 0.1f;
            return (Mathf.Abs(color1.r - color2.r) < threshold
                    && Mathf.Abs(color1.g - color2.g) < threshold
                    && Mathf.Abs(color1.b - color2.b) < threshold);
        }
    }
}