using UnityEngine;

namespace Deaven.TileLevelGenerator
{
    [SelectionBase]
    public class Tile : MonoBehaviour
    {
        public TileData tileData;
    
        [SerializeField] private GameObject occupied;
    
        public GameObject Occupied
        {
            get => occupied;
            set => occupied = value;
        }
    }   
}