using System.Collections.Generic;
using UnityEngine;

namespace Deaven.TileLevelGenerator
{
    [CreateAssetMenu(fileName = "Level", menuName = "TileLevelGenerator/Level", order = 0)]
    public class Level : ScriptableObject
    {
        public Texture2D layoutTexture;
        public Texture2D heightTexture;
        public int maxHeight = 1;
        public TileData defaultTile;
        public List<TileData> tiles = new List<TileData>();
    }
}