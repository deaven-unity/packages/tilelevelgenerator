using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.TileLevelGenerator
{
    [CreateAssetMenu(fileName = "Tile", menuName = "TileLevelGenerator/Tile", order = 0)]
    public class TileData : ScriptableObject
    {
        [SerializeField] private GameObject tilePrefab;
        [SerializeField] private bool useTileBeneath;

        [SerializeField, ShowIf("useTileBeneath")]
        private GameObject tileBeneathPrefab;

        [Tooltip("The color from the texture at which this tile will be created")]
        public Color color;

        [ShowInInspector] public string Hex => ColorUtility.ToHtmlStringRGB(color);

        public GameObject TilePrefab => tilePrefab;

        public GameObject TileBeneathPrefab => tileBeneathPrefab;
    }
}