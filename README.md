# Unity: Tile Level Generator

## How to install

* Open the Package Manager
* Plus-Icon: Add package from git URL...
* Use the HTTPS address found in "Clone"

# Usage

1. Create Empty Game Object and add Tile Level Generator Script
2. Create a Level: Project Folder > Right click > Create > TileLevelGenerator > Level
3. Assign Level to TileLevelGenerator
4. Create a Tile: Project Folder > Right click > Create > TileLevelGenerator > Tile
5. Create Tile Prefab
6. Add Tile Script to Prefab

# Extending



# Content
- Tile Level Generator
- Level 
- Tile
- Tile Data